﻿using UnityEngine;
using System.Collections;

public class MoveCube : MonoBehaviour {

	private Vector3 currentPos; 
	private Vector3 movePos;
	// Use this for initialization
	void Start () {
		currentPos = this.transform.position;
		setX(0);
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = movePos;
	
	}

	public void setX(float value){
		movePos = new Vector3(value,-17,0);
	}
}
