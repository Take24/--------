﻿using UnityEngine;
using System.Collections;

public class BulletGenerate : MonoBehaviour {


	public GameObject bullet;

	// Startメソッドをコルーチンとして呼び出す
	IEnumerator Start ()
	{
		while (true) {
			// 弾をプレイヤーと同じ位置/角度で作成
			Instantiate (bullet, transform.position, transform.rotation);
			audio.Play();
			// 0.05秒待つ
			yield return new WaitForSeconds (0.5f);

		}
	}
}
