﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	private float timer = 0;
	private bool gameEnd = false;

	public void setFlag(){
		gameEnd = !gameEnd;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(gameEnd){
			timer += Time.deltaTime;
			if(timer > 2.0f){
				Application.LoadLevel("Title");
			}
		}
	
	}
}
