﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public int speed = 10;

	// Use this for initialization
	void Start () {
		rigidbody2D.velocity = transform.up.normalized * speed;
	}

	void OnTriggerEnter2D(Collider2D c){
		string layerName = LayerMask.LayerToName(c.gameObject.layer);

		if( layerName != "DestroyArea"){
			Destroy(this.gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
