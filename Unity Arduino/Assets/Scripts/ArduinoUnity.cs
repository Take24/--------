﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System.Threading;

public class ArduinoUnity : MonoBehaviour {

	public GameObject cube;
	MoveCube moveCube;

	SerialPort stream = new SerialPort("COM3", 115200);

	private float time = 0;
	private string value  = "";
	private string[] sencer = new string[2];
	
	void Start () {
		moveCube = cube.GetComponent<MoveCube>();
		OpenConnection ();
	}
	void Update () {
		time += Time.deltaTime;
		if( time >= 0.05f ){
			try{
				value = stream.ReadLine();
				SetValue(value);
			}catch(System.TimeoutException){
				Debug.Log("owta");
			};
			time = 0;
		}
		
	}

	void SetValue(string s){
		sencer = s.Split(',');
		int x = int.Parse(sencer[0]);
		Debug.Log (sencer[0]+ "  :  " + sencer[1]);
		moveCube.setX(x);
	}
		
		void OnApplicationQuit(){
		stream.Close();
	}
	
	void OpenConnection() {
		
		if (stream != null) {
			
			if (stream.IsOpen) {
				stream.Close ();
				Debug.LogError ("Failed to open Serial Port, already open!");
			} else {
				stream.Open ();
				stream.ReadTimeout = 50;
				
				Debug.Log ("Open Serial port");      
			}
		}
	}
}
