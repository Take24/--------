﻿using UnityEngine;
using System.Collections;

public class MarkMove : MonoBehaviour {

	// Use this for initialization
	void Start () {

		// 4秒かけて、y軸を260度回転
		iTween.RotateTo(gameObject, iTween.Hash("z", 720, "time", 2.0f));
		// 4秒かけて、y軸を3倍に拡大
		iTween.ScaleTo(gameObject, iTween.Hash("x",1,"y",1, "time", 2.0f));

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
