﻿using UnityEngine;
using System.Collections;

public class MarkGenerate : MonoBehaviour {

	//xperia z1 default(Don't use)
	//public int height = 1920;
	//public int width  = 1020;

	public GameObject[] Mark = new GameObject[3];

	private float timer;
	private float interval;
	private int   count;

	//Generate Area
	private float _height = 5.0f;
	private float _width  = 3.5f;

	// Use this for initialization
	void Start () {

		timer = 0.0f;
		interval = 2.0f;
		count = 0;
	
	}
	
	// Update is called once per frame
	void Update () {

		timer += Time.deltaTime;
		if( timer >= interval ){
			//Mark generate
			Instantiate(Mark[Random.Range(0,3)],
			            new Vector3( Random.Range(-_width,_width) , Random.Range(-_height,_height) , transform.position.z ),
			            transform.rotation);
			timer =  0.0f;
			count += 1;

			if( count % 10 == 0 ){
				interval -= 0.2f;
			}
		}
	
	}
}
